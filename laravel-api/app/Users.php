<?php

namespace App;

use Tymon\JWTAuth\Contracts\JWTSubject;
use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Support\Str;

class Users extends Authenticatable implements JWTSubject
{
    use Notifiable;

    // Rest omitted for brevity

    /**
     * Get the identifier that will be stored in the subject claim of the JWT.
     *
     * @return mixed
     */
    public function getJWTIdentifier()
    {
        return $this->getKey();
    }

    /**
     * Return a key value array, containing any custom claims to be added to the JWT.
     *
     * @return array
     */
    public function getJWTCustomClaims()
    {
        return [];
    }
    protected $fillable = ['username', 'email', 'name', 'role_id', 'password', 'email_verified_at'];
    // primary key ubah
    protected $primaryKey = 'id';
    // type id diubah jadi string
    protected $keyType = 'string';
    // mematikan auto increment
    public $incrementing = false;

    // menjalankan sistem uuid
    protected static function boot()
    {
        // memanggil boot yang sudaha da
        parent::boot();
        // menjalankan proses creating
        static::creating(function($model){
            if (empty($model->{$model->getKeyName()})){
                $model->{$model->getKeyName()} = Str::uuid();
            }
        });
    }

    // menyambungkan dengan users
    public function roles()
    {
        return $this->belongsTo('App\Roles', 'role_id');
    }
    public function otpCode(){
        return $this->hasOne('App\OtpCode', 'user_id');
    }
    // menyambungkan dengan 
    public function comments()
    {
        return $this->hasMany('App\Comments');
    }
    // menyambungkan dengan 
    public function posts()
    {
        return $this->hasMany('App\Posts');
    }
}
