<?php

namespace App;

use Illuminate\Support\Str;
use Illuminate\Database\Eloquent\Model;

class OtpCode extends Model
{
    protected $guarded = [];
    // primary key ubah
    protected $primaryKey = 'id';
    // type id diubah jadi string
    protected $keyType = 'string';
    // mematikan auto increment
    public $incrementing = false;

    // menjalankan sistem uuid
    protected static function boot()
    {
        // memanggil boot yang sudaha da
        parent::boot();
        // menjalankan proses creating
        static::creating(function($model){
            if (empty($model->{$model->getKeyName()})){
                $model->{$model->getKeyName()} = Str::uuid();
            }
        });
    }
    public function users()
    {
        return $this->belongsTo('App\Users','user_id');
    }
}
