<?php

namespace App\Events;

use App\Comments;
use Illuminate\Broadcasting\Channel;
use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Broadcasting\PresenceChannel;
use Illuminate\Broadcasting\PrivateChannel;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Queue\SerializesModels;

class CommentStored
{
    use Dispatchable, InteractsWithSockets, SerializesModels;

    public $comments;
    /**
     * Create a new event instance.
     *
     * @return void
     */
    public function __construct(Comments $comments)
    {
        $this->comments = $comments;
    }

}
