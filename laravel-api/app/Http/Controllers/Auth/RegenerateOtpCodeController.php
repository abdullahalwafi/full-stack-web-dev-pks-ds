<?php

namespace App\Http\Controllers\Auth;

use App\Users;
use App\OtpCode;
use Carbon\Carbon;
use App\Mail\RegenerateMail;
use Illuminate\Http\Request;
use App\Events\RegenerateStored;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Validator;

class RegenerateOtpCodeController extends Controller
{
    /**
     * Handle the incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function __invoke(Request $request)
    {
        //Buat validasi
        $validator = Validator::make($request->all(), [
            'email'   => 'required',
        ]);

        //kasih tau error validator
        if ($validator->fails()) {
            return response()->json($validator->errors(), 400);
        }
        // mencari user berdasarkan email
        $users = Users::where('email', $request->email)->first();
        // Menghapus otp
        if ($users->otpCode) {
            $users->otpCode->delete();
        }

        do {
            $otp = mt_rand(100000, 999999);
            $check = OtpCode::where('otp', $otp)->first();
        } while ($check);

        $validuntil = Carbon::now()->addMinute(5);
        $otp_code = OtpCode::create([
            'otp'           => $otp,
            'valid_until'   => $validuntil,
            'user_id'   => $users->id
        ]);


        // panggil event RegerateStored
        event(new RegenerateStored($otp_code));
        // Mail::to($otp_code->users->email)->send(new RegenerateMail($otp_code));

        // kalo berhasil
        return response()->json([
            'success' => true,
            'message' => 'Otp telah dibuat ulang silahkan cek email anda',
            'data' => [
                'users' => $users,
                'otp_code' => $otp_code
            ]
        ], 200);
        //kalo gagal
        return response()->json([
            'success' => false,
            'message' => 'Data Failed to Save',
        ], 409);
    }
}
