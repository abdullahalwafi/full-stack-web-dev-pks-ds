<?php

namespace App\Http\Controllers\Auth;

use App\Users;
use App\OtpCode;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;

class VerificationController extends Controller
{
    /**
     * Handle the incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function __invoke(Request $request)
    {
        //Buat validasi
        $validator = Validator::make($request->all(), [
            'otp'   => 'required',
        ]);

        //kasih tau error validator
        if ($validator->fails()) {
            return response()->json($validator->errors(), 400);
        }
        // cek didatabase ada otp ga
        $otpCode = OtpCode::where('otp', $request->otp)->first();
        // kita ambil user sesuai otp
        $users = $otpCode->users;

        // cek masa aktif otp
        if ($otpCode->valid_until < Carbon::now()){
            return response()->json([
                'success' => false,
                'message' => 'Otp Anda sudah kadarluasa',
            ], 404);
        }
        $users->update([
            'email_verified_at' => Carbon::now(),
        ]);
        $otpCode->delete();

        return response()->json([
            'success' => true,
            'message' => 'Selamat akun anda terverifikasi',
            'data' => $users
        ]);
    }
}
