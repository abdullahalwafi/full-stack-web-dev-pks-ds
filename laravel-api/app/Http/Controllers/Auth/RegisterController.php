<?php

namespace App\Http\Controllers\Auth;

use App\Events\RegisterStored;
use App\Users;
use App\OtpCode;
use Carbon\Carbon;
use App\Mail\RegisterMail;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Validator;

class RegisterController extends Controller
{
    /**
     * Handle the incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function __invoke(Request $request)
    {
        //Buat validasi
        $validator = Validator::make($request->all(), [
            'name'   => 'required',
            'email' => 'required|unique:users,email|email',
            'username' => 'required|unique:users,username',
        ]);

        //kasih tau error validator
        if ($validator->fails()) {
            return response()->json($validator->errors(), 400);
        }
        // nambah user ke database
        $users = Users::create($request->all());

        do {
            $otp = mt_rand(100000, 999999);
            $check = OtpCode::where('otp', $otp)->first();
        } while ($check);

        $validuntil = Carbon::now()->addMinute(5);
        $otp_code = OtpCode::create([
            'otp'           => $otp,
            'valid_until'   => $validuntil,
            'user_id'   => $users->id
        ]);

        // panggil event RegisterStored
        event(new RegisterStored($otp_code));
        // Mail::to($otp_code->users->email)->send(new RegisterMail($otp_code));
        // kalo berhasil
        return response()->json([
            'success' => true,
            'message' => 'Data user berhasil di dibuat, Silahkan Verifikasi via email',
            'data' => [
                'users' => $users,
                'otp_code' => $otp_code
            ]
        ], 200);
        //kalo gagal
        return response()->json([
            'success' => false,
            'message' => 'Data Failed to Save',
        ], 409);
    }
}
