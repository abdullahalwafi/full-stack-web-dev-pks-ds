<?php

namespace App\Http\Controllers\Auth;

use App\Users;
use App\OtpCode;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;

class UpdatePasswordController extends Controller
{
    /**
     * Handle the incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function __invoke(Request $request)
    {
        //Buat validasi
        $validator = Validator::make($request->all(), [
            'email'   => 'required',
            'password'   => 'required|confirmed|min:6',
        ]);

        //kasih tau error validator
        if ($validator->fails()) {
            return response()->json($validator->errors(), 400);
        }
        // cari user
        $users = Users::where('email', $request->email)->first();

        // update password
        $users->update([
            'password' => Hash::make($request->password)
        ]);

        // kalo berhasil
        return response()->json([
            'success' => true,
            'message' => 'Password berhasil di Update silahkan login',
            'data' => $users
        ]) ;
        //kalo gagal
        return response()->json([
            'success' => false,
            'message' => 'Passwod gagal diupdate',
        ], 409);
    }
}
