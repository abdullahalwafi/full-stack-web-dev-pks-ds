<?php

namespace App\Http\Controllers\Auth;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;
use App\Users;
class LoginController extends Controller
{
    /**
     * Handle the incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function __invoke(Request $request)
    {
        //Buat validasi
        $validator = Validator::make($request->all(), [
            'email'   => 'required',
            'password' => 'required',
        ]);

        //kasih tau error validator
        if ($validator->fails()) {
            return response()->json($validator->errors(), 400);
        }
        $credentials = request(['email', 'password']);

        if (! $token = auth()->attempt($credentials)) {
            return response()->json([
                'success' => false,
                'message' => 'Email atau password anda tidak sesuai',
            ], 401);
        }

        return response()->json([
            'success' => true,
            'message' => 'Anda berhasil login',
            'data'    => [
                'users' => auth()->user(),
                'token' => $token
            ] 
        ]);
    }
}
