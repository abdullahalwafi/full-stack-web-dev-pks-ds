<?php

namespace App\Http\Controllers;

use App\Posts;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class PostsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $posts = Posts::latest()->get();

        return response()->json([
            'success' => true,
            'message' => 'Data post berhasil di tampilkan',
            'data' => $posts,
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //Buat validasi
        $validator = Validator::make($request->all(), [
            'title'   => 'required',
            'content' => 'required',
        ]);

        //kasih tau error validator
        if ($validator->fails()) {
            return response()->json($validator->errors(), 400);
        }
        // nambah data ke database
        $posts = Posts::create([
            'title' => $request->title,
            'content' => $request->content,
        ]);
        // kalo berhasil
        return response()->json([
            'success' => true,
            'message' => 'Data post berhasil di tambahkan',
            'data' => $posts,
        ], 200);
        //kalo gagal
        return response()->json([
            'success' => false,
            'message' => 'Post Failed to Save',
        ], 409);
    }


    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //find post by ID
        $posts = Posts::findOrfail($id);

        //make response JSON
        return response()->json([
            'success' => true,
            'message' => 'Detail Data Post',
            'data'    => $posts
        ], 200);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //set validation
        $validator = Validator::make($request->all(), [
            'title'   => 'required',
            'content' => 'required',
        ]);

        //response error validation
        if ($validator->fails()) {
            return response()->json($validator->errors(), 400);
        }

        //find post by ID
        $posts = Posts::findOrFail($id);

        if ($posts) {
            // cek user yg ngedit itu sesuai ga
            $user = auth()->user();
            if ($posts->user_id != $user->id) {
                //data posts not found
                return response()->json([
                    'success' => false,
                    'message' => 'Data bukan milik anda',
                ], 403);
            }

            //update posts
            $posts->update([
                'title'     => $request->title,
                'content'   => $request->content
            ]);
            return response()->json([
                'success' => true,
                'message' => 'posts Updated',
                'data'    => $posts
            ], 200);
        }

        //data posts not found
        return response()->json([
            'success' => false,
            'message' => 'posts Not Found',
        ], 404);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //cara id
        $posts = Posts::findOrfail($id);

        if ($posts) {
            // cek user yg ngedit itu sesuai ga
            $user = auth()->user();
            if ($posts->user_id != $user->id) {
                //data posts not found
                return response()->json([
                    'success' => false,
                    'message' => 'Data bukan milik anda',
                ], 403);
            }
            //delete posts
            $posts->delete();

            return response()->json([
                'success' => true,
                'message' => 'posts Deleted',
            ], 200);
        }

        //data posts not found
        return response()->json([
            'success' => false,
            'message' => 'posts Not Found',
        ], 404);
    }
}
