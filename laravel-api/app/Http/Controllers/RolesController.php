<?php

namespace App\Http\Controllers;

use App\Roles;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class RolesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $roles = Roles::latest()->get();

        return response()->json([
            'success' => true,
            'message' => 'Data Roles berhasil di tampilkan',
            'data' => $roles,
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //Buat validasi
        $validator = Validator::make($request->all(), [
            'name'   => 'required',
        ]);
        
        //kasih tau error validator
        if ($validator->fails()) {
            return response()->json($validator->errors(), 400);
        }
        // nambah data ke database
        $roles = Roles::create([
            'name' => $request->name,
        ]);
        // kalo berhasil
        return response()->json([
            'success' => true,
            'message' => 'Data Roles berhasil di tambahkan',
            'data' => $roles,
        ], 200);
         //kalo gagal
         return response()->json([
            'success' => false,
            'message' => 'Roles Failed to Save',
        ], 409);
    }
    

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //find Roles by ID
        $roles = Roles::findOrfail($id);

        //make response JSON
        return response()->json([
            'success' => true,
            'message' => 'Detail Data Roles',
            'data'    => $roles
        ], 200);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //set validation
        $validator = Validator::make($request->all(), [
            'name'   => 'required',
        ]);
        
        //response error validation
        if ($validator->fails()) {
            return response()->json($validator->errors(), 400);
        }

        //find Roles by ID
        $roles = Roles::findOrFail($id);

        if($roles) {

            //update roles
            $roles->update([
                'name'     => $request->name,
            ]);

            return response()->json([
                'success' => true,
                'message' => 'roles Updated',
                'data'    => $roles  
            ], 200);

        }

        //data roles not found
        return response()->json([
            'success' => false,
            'message' => 'roles Not Found',
        ], 404);

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //cara id
        $roles = Roles::findOrfail($id);

        if($roles) {

            //delete roles
            $roles->delete();

            return response()->json([
                'success' => true,
                'message' => 'roles Deleted',
            ], 200);

        }

        //data roles not found
        return response()->json([
            'success' => false,
            'message' => 'roles Not Found',
        ], 404);
    }
}
