<?php

namespace App\Http\Controllers;

use App\Comments;
use App\Events\CommentStored;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class CommentsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $comments = Comments::latest()->get();

        return response()->json([
            'success' => true,
            'message' => 'Data comments berhasil di tampilkan',
            'data' => $comments,
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //Buat validasi
        $validator = Validator::make($request->all(), [
            'content'   => 'required',
            'post_id' => 'required',
        ]);

        //kasih tau error validator
        if ($validator->fails()) {
            return response()->json($validator->errors(), 400);
        }
        // nambah data ke database
        $comments = Comments::create([
            'content' => $request->content,
            'post_id' => $request->post_id,
        ]);

        // panggil event CommentStoredEvent
        event(new CommentStored($comments));

        // // ini dikirim kepada yang memiliki posts
        // Mail::to($comments->posts->users->email)->send(new NotifPostAuthorMail($comments));

        // // ini dikirim ke yang komen
        // Mail::to($comments->users->email)->send(new NotifCommentAuthorMail($comments));

        if ($comments) {
            // kalo berhasil
            return response()->json([
                'success' => true,
                'message' => 'Data Comments berhasil di tambahkan',
                'data' => $comments,
            ], 200);
            //kalo gagal
            return response()->json([
                'success' => false,
                'message' => 'Comments Failed to Save',
            ], 409);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //find post by ID
        $comments = Comments::findOrfail($id);

        //make response JSON
        return response()->json([
            'success' => true,
            'message' => 'Detail Comments Post',
            'data'    => $comments
        ], 200);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //set validation
        $validator = Validator::make($request->all(), [
            'content'   => 'required',
            'post_id' => 'required',
        ]);

        //response error validation
        if ($validator->fails()) {
            return response()->json($validator->errors(), 400);
        }

        //find commentsby ID
        $comments = Comments::findOrFail($id);

        if ($comments) {

            //update comments
            $comments->update([
                'content'     => $request->content,
                'post_id'   => $request->post_id
            ]);

            return response()->json([
                'success' => true,
                'message' => 'comments Updated',
                'data'    => $comments
            ], 200);
        }

        //data comments not found
        return response()->json([
            'success' => false,
            'message' => 'comments Not Found',
        ], 404);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //cara id
        $comments = Comments::findOrfail($id);

        if ($comments) {

            //delete comments
            $comments->delete();

            return response()->json([
                'success' => true,
                'message' => 'comments Deleted',
            ], 200);
        }

        //data comments not found
        return response()->json([
            'success' => false,
            'message' => 'comments Not Found',
        ], 404);
    }
}
