<?php

namespace App\Http\Controllers;

use App\Users;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class UsersController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $users = Users::latest()->get();

        return response()->json([
            'success' => true,
            'message' => 'Data users berhasil di tampilkan',
            'data' => $users,
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //Buat validasi
        $validator = Validator::make($request->all(), [
            'username'   => 'required',
            'email' => 'required',
            'name' => 'required',
            'role_id' => 'required',
        ]);

        //kasih tau error validator
        if ($validator->fails()) {
            return response()->json($validator->errors(), 400);
        }
        // nambah data ke database
        $users = Users::create([
            'username' => $request->username,
            'email' => $request->email,
            'name' => $request->name,
            'role_id' => $request->role_id
        ]);
        // kalo berhasil
        return response()->json([
            'success' => true,
            'message' => 'Data users berhasil di tambahkan',
            'data' => $users,
        ], 200);
        //kalo gagal
        return response()->json([
            'success' => false,
            'message' => 'users Failed to Save',
        ], 409);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //find post by ID
        $users = Users::findOrfail($id);

        //make response JSON
        return response()->json([
            'success' => true,
            'message' => 'Detail users Post',
            'data'    => $users
        ], 200);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //set validation
        $validator = Validator::make($request->all(), [
            'username'   => 'required',
            'email' => 'required',
            'name' => 'required',
            'role_id' => 'required',
        ]);

        //response error validation
        if ($validator->fails()) {
            return response()->json($validator->errors(), 400);
        }

        //find usersby ID
        $users = Users::findOrFail($id);

        if ($users) {

            //update users
            $users->update([
                'username' => $request->username,
                'email' => $request->email,
                'name' => $request->name,
                'role_id' => $request->role_id
            ]);

            return response()->json([
                'success' => true,
                'message' => 'users Updated',
                'data'    => $users
            ], 200);
        }

        //data users not found
        return response()->json([
            'success' => false,
            'message' => 'users Not Found',
        ], 404);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //cara id
        $users = Users::findOrfail($id);

        if ($users) {

            //delete users
            $users->delete();

            return response()->json([
                'success' => true,
                'message' => 'users Deleted',
            ], 200);
        }

        //data users not found
        return response()->json([
            'success' => false,
            'message' => 'users Not Found',
        ], 404);
    }
}
