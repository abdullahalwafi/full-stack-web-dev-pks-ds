<?php

namespace App\Listeners;

use App\Mail\RegenerateMail;
use App\Events\RegenerateStored;
use Illuminate\Support\Facades\Mail;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

class SendOtpCodeRegenerate implements ShouldQueue
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  RegenerateStored  $event
     * @return void
     */
    public function handle(RegenerateStored $event)
    {
        Mail::to($event->otp_code->users->email)->send(new RegenerateMail($event->otp_code));
    }
}
