<?php

namespace App\Listeners;

use App\Events\CommentStored;
use App\Mail\NotifCommentAuthorMail;
use Illuminate\Support\Facades\Mail;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

class SendEmailToCommentAuthor implements ShouldQueue
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  CommentStored  $event
     * @return void
     */
    public function handle(CommentStored $event)
    {
        Mail::to($event->comments->users->email)->send(new NotifCommentAuthorMail($event->comments));
    }
}
