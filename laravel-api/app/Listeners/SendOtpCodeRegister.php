<?php

namespace App\Listeners;

use App\Events\RegisterStored;
use App\Mail\RegisterMail;
use Illuminate\Support\Facades\Mail;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

class SendOtpCodeRegister implements ShouldQueue
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  RegisterStored  $event
     * @return void
     */
    public function handle(RegisterStored $event)
    {
        Mail::to($event->otp_code->users->email)->send(new RegisterMail($event->otp_code));
    }
}
