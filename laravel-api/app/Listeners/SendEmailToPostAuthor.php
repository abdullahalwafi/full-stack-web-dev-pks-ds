<?php

namespace App\Listeners;

use App\Events\CommentStored;
use App\Mail\NotifPostAuthorMail;
use Illuminate\Support\Facades\Mail;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

class SendEmailToPostAuthor implements ShouldQueue
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  CommentStored  $event
     * @return void
     */
    public function handle(CommentStored $event)
    {
        Mail::to($event->comments->posts->users->email)->send(new NotifPostAuthorMail($event->comments));
    }
}
