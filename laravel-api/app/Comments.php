<?php

namespace App;

use Illuminate\Support\Str;
use Illuminate\Database\Eloquent\Model;

class Comments extends Model
{
    protected $fillable = ['content', 'post_id', 'user_id'];
    // primary key ubah
    protected $primaryKey = 'id';
    // type id diubah jadi string
    protected $keyType = 'string';
    // mematikan auto increment
    public $incrementing = false;

    // menjalankan sistem uuid
    protected static function boot()
    {
        // memanggil boot yang sudaha da
        parent::boot();
        // menjalankan proses creating
        static::creating(function($model){
            if (empty($model->{$model->getKeyName()})){
                $model->{$model->getKeyName()} = Str::uuid();
            }
            $model->user_id = auth()->user()->id;
        });
    }

    // menyambungkan dengan 
    public function posts()
    {
        return $this->belongsTo('App\Posts', 'post_id');
    }
    // menyambungkan dengan 
    public function users()
    {
        return $this->belongsTo('App\Users', 'user_id');
    }
}
