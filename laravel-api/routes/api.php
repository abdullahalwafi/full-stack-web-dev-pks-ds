<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware(['auth:api'])->group(function () {
    Route::apiResource('/posts', 'PostsController')->only(['store', 'update', 'destroy']);
    Route::apiResource('/comments', 'CommentsController')->only(['store', 'update', 'destroy']);
});


Route::apiResource('/posts', 'PostsController')->only(['index', 'show']);
Route::apiResource('/comments', 'CommentsController')->only(['index', 'show']);
Route::apiResource('/users', 'UsersController');
Route::apiResource('/roles', 'RolesController');

Route::group(['prefix' => 'auth', 'namespace' => 'auth'], function () {
    Route::post('/register', 'RegisterController')->name('auth.register');
    Route::post('/regenerate-otp-code', 'RegenerateOtpCodeController')->name('auth.regenerate');
    Route::post('/verification', 'VerificationController')->name('auth.verification');
    Route::post('/update-password', 'UpdatePasswordController')->name('auth.update_password');
    Route::post('/login', 'LoginController')->name('auth.login');
});
