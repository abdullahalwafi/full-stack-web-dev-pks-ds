<?php 

// Hewan Class
class Hewan{
    public $nama;
    public $darah = 50;
    public $jumlahKaki;
    public $keahlian;

    public function __construct($nama, $jumlahKaki , $keahlian, $attackPower, $defencePower)
    {
        $this->nama = $nama;
        $this->jumlahKaki = $jumlahKaki;
        $this->keahlian = $keahlian;
        $this->attackPower = $attackPower;
        $this->defencePower = $defencePower;
    }
    public function atraksi()
    {
        return "Ketika " . $this->nama . " sedang " . $this->keahlian.". ";
    }
    
}
// Fight Class
trait Fight 
{
    public $attackPower;
    public $defencePower;
    public function serang($diserang, $penyerang)
    {
        echo $this->nama." menyerang ".$diserang->nama."<br>";
        $diserang->diserang($penyerang);
    }

    public function diserang($penyerang)
    {
        $this->darahakhir = $this->darah - $penyerang->attackPower/$this->defencePower;
        echo "saat itu " . $this->nama." sedang diserang ".$penyerang->nama." mengakibatkan darahnya sisa ".$this->darahakhir.".<br><br>";
        
    }
}
class Elang extends Hewan
{
    use Fight;
    public function getInfoHewan()
    {
        echo "Nama Hewan : ".$this->nama."<br> Darah Awal :".$this->darah."<br> Keahlian : ".$this->keahlian."<br> Jumlah Kaki : ".$this->jumlahKaki."<br> Attack Power : ".$this->attackPower."<br>Defense Power : ".$this->defencePower."<br>".$this->nama." merupakan jenis dari ".get_class($this)."<br><br>";
    }

}
class Harimau extends Hewan
{
    use Fight;
    public function getInfoHewan()
    {
        echo "Nama Hewan :".$this->nama."<br> Darah Awal : ".$this->darah."<br> Keahlian : ".$this->keahlian."<br> Jumlah Kaki : ".$this->jumlahKaki."<br> Attack Power :".$this->attackPower."<br>Defense Power :".$this->defencePower."<br>".$this->nama." merupakan jenis dari ".get_class($this)."<br><br>";
    }

}
$elang = new Elang("Elang Bondol", 2, "terbang tinggi", 10, 5);
$harimau = new Harimau("Harimau Sumatera", 4, "lari cepat", 7, 8);

$elang->getInfoHewan() ;
echo $harimau->atraksi();
$elang->serang($harimau, $elang);

$harimau->getInfoHewan();
echo $elang->atraksi();
$harimau->serang($elang, $harimau);



?>