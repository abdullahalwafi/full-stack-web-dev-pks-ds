// Soal Nomor 1
// luas Persegi panjang
const LuasPersegiPanjang = (p, l) =>
{
    let hasilLuas = p * l
    return hasilLuas
}

// Luas
const KelilingPersegiPanjang = (p, l) => {
    let hasilKeliling = 2 * (p + l)
    return hasilKeliling
}

// soal Nomor 2
const newFunction = (firstName, lastName) => {

  return {
    firstName,
    lastName,
    fullName:  () =>{
      console.log(`${firstName}` + " " + `${lastName}`)
    }
  }
}
 
//Driver Code 
newFunction("William", "Imoh").fullName()   


// Soal nomor 3
const newObject = {
    firstName: "Muhammad",
    lastName: "Iqbal Mubarok",
    address: "Jalan Ranamanyar",
    hobby: "playing football",
}
  
const {firstName, lastName, address, hobby} = newObject
//Driver Code
console.log(firstName, lastName, address, hobby)

// soal nomor 4
const west = ["Will", "Chris", "Sam", "Holly"]
const east = ["Gill", "Brian", "Noel", "Maggie"]
const combined = [...west, ...east]
//Driver Code
console.log(combined)

// Soal Nomor 5
const planet = "earth" 
const view = "glass" 
const before = 'Lorem ' + `${view}` + ' dolor sit amet, ' + 'consectetur adipiscing elit,' + `${planet}` 
//Driver Code
console.log(before)
