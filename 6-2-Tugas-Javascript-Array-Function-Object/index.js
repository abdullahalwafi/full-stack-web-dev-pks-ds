// Soal pertama
function loopSorting(array) {
    for (let i = 0; i < array.length; i++) {
        array.forEach(element => {
            if (element[0]==i+1) {
                hewanSort.push(element)
            }
        });
    }
}
function show(parameters) {
    parameters.forEach(element => {
        console.log(element)
    });
}

console.log("\nIni Jawaban Soal Pertama")
var daftarHewan = [
    "2. Komodo",
    "5. Buaya",
    "3. Cicak",
    "4. Ular",
    "1. Tokek"
];
var hewanSort = []
loopSorting(daftarHewan)
show(hewanSort)

// Soal Kedua
function introduce(parameters) {
    let introduce = `Nama saya ${parameters.name}, umur saya ${parameters.age} tahun, alamat saya di ${parameters.address}, dan saya punya hobby yaitu ${parameters.hobby}`
    return introduce
}
console.log("\nIni Jawaban Soal Kedua")
var data = {name : "John" , age : 30 , address : "Jalan Pelesiran" , hobby : "Gaming" }
var perkenalan = introduce(data)
console.log(perkenalan)

// Soal Ketiga
function hitung_huruf_vokal(parameters) {
    data = parameters.toLowerCase()
    let vokal = ['a','i','u','e','o']
    let hitung = 0
    for (let i = 0; i < data.length; i++) {
        for (let j = 0; j < vokal.length; j++) {
            if (data[i]==vokal[j]) {
                hitung += 1
            }
        }
    }
    return hitung
}

console.log("\nIni Jawaban Soal Ketiga")
var hitung_1 = hitung_huruf_vokal("Muhammad")
var hitung_2 = hitung_huruf_vokal("Iqbal")
console.log(hitung_1 , hitung_2)

// Soal Keempat
function hitung(n) {
    return (n-1)*2
}
console.log("\nIni Jawaban Soal Keempat")
console.log( hitung(0) )
console.log( hitung(1) )
console.log( hitung(2) )
console.log( hitung(3) )
console.log( hitung(5) )
